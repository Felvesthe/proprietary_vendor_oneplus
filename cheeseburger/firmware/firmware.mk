# Firmware
PRODUCT_COPY_FILES += \
    vendor/oneplus/cheeseburger/firmware/static_nvbk.bin:install/firmware-update/static_nvbk.bin \
    vendor/oneplus/cheeseburger/firmware/abl.elf:install/firmware-update/abl.elf \
    vendor/oneplus/cheeseburger/firmware/adspso.bin:install/firmware-update/adspso.bin \
    vendor/oneplus/cheeseburger/firmware/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/oneplus/cheeseburger/firmware/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/oneplus/cheeseburger/firmware/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/oneplus/cheeseburger/firmware/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/oneplus/cheeseburger/firmware/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/oneplus/cheeseburger/firmware/keymaster.mbn:install/firmware-update/keymaster.mbn \
    vendor/oneplus/cheeseburger/firmware/logo.bin:install/firmware-update/logo.bin \
    vendor/oneplus/cheeseburger/firmware/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/oneplus/cheeseburger/firmware/pmic.elf:install/firmware-update/pmic.elf \
    vendor/oneplus/cheeseburger/firmware/rpm.mbn:install/firmware-update/rpm.mbn \
    vendor/oneplus/cheeseburger/firmware/tz.mbn:install/firmware-update/tz.mbn \
    vendor/oneplus/cheeseburger/firmware/xbl.elf:install/firmware-update/xbl.elf
