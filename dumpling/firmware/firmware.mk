# Firmware
PRODUCT_COPY_FILES += \
    vendor/oneplus/dumpling/firmware/static_nvbk.bin:install/firmware-update/static_nvbk.bin \
    vendor/oneplus/dumpling/firmware/abl.elf:install/firmware-update/abl.elf \
    vendor/oneplus/dumpling/firmware/adspso.bin:install/firmware-update/adspso.bin \
    vendor/oneplus/dumpling/firmware/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/oneplus/dumpling/firmware/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/oneplus/dumpling/firmware/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/oneplus/dumpling/firmware/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/oneplus/dumpling/firmware/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/oneplus/dumpling/firmware/keymaster.mbn:install/firmware-update/keymaster.mbn \
    vendor/oneplus/dumpling/firmware/logo.bin:install/firmware-update/logo.bin \
    vendor/oneplus/dumpling/firmware/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/oneplus/dumpling/firmware/pmic.elf:install/firmware-update/pmic.elf \
    vendor/oneplus/dumpling/firmware/rpm.mbn:install/firmware-update/rpm.mbn \
    vendor/oneplus/dumpling/firmware/tz.mbn:install/firmware-update/tz.mbn \
    vendor/oneplus/dumpling/firmware/xbl.elf:install/firmware-update/xbl.elf
